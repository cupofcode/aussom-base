/*
 * Copyright 2021 Austin Lehman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The static base64 class provides functions for base64
 * encoding and decoding.
 */
static extern class base64 : com.aussom.stdlib.ABase64 {
    /**
     * Converts binary buffer object to base64 encoded hex string.
     * @p BufferObj is the binary buffer object to convert.
     * @r An encoded string.
     */
	public extern encode(object BufferObj);

	/**
	 * Converts binary buffer object to raw base64 encoded string.
	 * @p BufferObj is the binary buffer object to convert.
     * @r An encoded string.
	 */
	public extern encodeRaw(object BufferObj);

	/**
	 * Converts base64 encoded hex string to binary buffer object.
	 * @p B64EncodedString is a base64 encoded string.
	 * @r A binary buffer object with the result.
	 */
	public extern decode(string B64EncodedString);

	/**
	 * Converts base64 encoded raw string to binary buffer object.
	 * @p B64EncodedString is a base64 encoded string.
	 * @r A binary buffer object with the result.
	 */
	public extern decodeRaw(string B64EncodedString);
}

/**
 * The static hex class provides functions for hex
 * encoding and decoding.
 */
static extern class hex : com.aussom.stdlib.AHex
{
    /**
     * Converts binary buffer object to hex string.
     * @p BufferObj is a binary buffer object to convert.
     * @r A hex encoded string.
     */
	public extern encode(object BufferObj);

	/**
	 * Converts hex string to binary buffer object.
	 * @p HexEncodedString is a string to encode.
	 * @r A binary buffer object with the decoded value.
	 */
	public extern decode(string HexEncodedString);
}

/**
 * The static uuid class provides universal ID
 * creation functionality.
 */
static extern class uuid : com.aussom.stdlib.AUuid
{
    /**
     * Standard globally unique id.
     * @r A string with the generated UUID.
     */
	public extern get();

	/**
	 * Generates a globally unique id.
     * Uses SHA-1 to reduce predictability.
     * @r A string with the generated UUID.
     */
	public extern getSecure();
}

/**
 * The static regex class provides various regular
 * expression functionality. Aussom uses Java
 * regular expressions.
 */
static extern class regex : com.aussom.stdlib.ARegex
{
	/**
	 * Returns a list of string matches.
	 * @p RegexStr is a string with the regular expression.
	 * @p Haystack is a string to search.
	 * @r A list of strings with the match results.
	 */
	public extern match(string RegexStr, string Haystack);

	/**
	 * Returns a string with the match,
	 * or null if no matches found.
	 * @p RegexStr is a string with the regular expression.
	 * @p Haysack is a string to search.
	 * @r A string with the first match if found or null if not.
	 */
	public matchFirst(string RegexStr, string Haystack) {
		matches = regex.match(RegexStr, Haystack);
		if(#matches > 0) { return matches[0]; }
		else { return null; }
	}

	/**
	 * Returns a string with the match,
	 * or null if no matches found.
	 * @p RegexStr is a string with the regular expression.
	 * @p Haysack is a string to search.
	 * @r A string with the last match if found or null if not.
	 */
	public matchLast(string RegexStr, string Haystack) {
		matches = regex.match(RegexStr, Haystack);
		count = #matches;
		if(#matches > 0) { return matches[(count - 1)]; }
		else { return null; }
	}

	/**
	 * Replaces all occurrences with replacement string.
	 * @p RegexStr is a string with the regular expression.
	 * @p ReplaceStr is a string with the value to replace.
	 * @p Haysack is a string to search.
	 * @r A string that's been replaced.
	 */
	public extern replace(string RegexStr, string ReplaceStr, string Haystack);

	/**
	 * Replaces first occurrence with replacement string.
	 * @p RegexStr is a string with the regular expression.
	 * @p ReplaceStr is a string with the value to replace.
	 * @p Haysack is a string to search.
	 * @r A string that's been replaced.
	 */
	public extern replaceFirst(string RegexStr, string ReplaceStr, string Haystack);
}