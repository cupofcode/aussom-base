/*
 * Copyright 2021 Austin Lehman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Reflect class provides functions that allow introspection
 * of the Aussom interpreter. These functions include viewing
 * loaded modules, classes, and class definitions. It include
 * invoking functions, evaling code, and loading modules, but
 * some of these functions required special permissions in
 * the runtime that may not be granted.
 */
static extern class reflect : com.aussom.stdlib.AReflect {
	/**
	 * Evaluates the provided Aussom code string. Using this function
	 * requires 'reflect.eval.string' security manager permission.
	 * @p CodeStr is a string with the code to evaluate.
	 * @p Name is an optional name to be used as the file
	 * name of the code.
	 * @r This object.
	 */
	public extern evalStr(string CodeStr, string Name = "evalStr");

	/**
	 * Evaluates the provided Aussom code file. Using this function
	 * requires 'reflect.eval.file' security manager permission.
	 * @p FileName is a string with the file name to evaluate.
	 * @r This object.
	 */
	public extern evalFile(string FileName);

	/**
	 * Includes the module with the provided module name. This is
	 * similar to evalFile but instead of specifying a file the module
	 * name is specified and the module file will be looked for in the
	 * normal include directories. Using the function requires
	 * 'reflect.include.module' security manager permission.
	 * @p ModuleName is a string with the module name to include.
	 * @r This object.
	 */
	public extern includeModule(string ModuleName);

	/**
	 * Gets a list of the loaded Aussom modules.
	 * @r A list of strings with the module names loaded.
	 */
	public extern loadedModules();

	/**
	 * Gets a list of the loaded Aussom classes.
	 * @r A list of strings with the class names loaded.
	 */
	public extern loadedClasses();

	/**
	 * Checks to see if the provided module name has been loaded.
	 * @p ModuleName is a string with the module name to check.
	 * @r A boolean with true if loaded and false if not.
	 */
	public extern isModuleLoaded(string ModuleName);

	/**
	 * Checks to see if the provided class name exists.
	 * @p ClassName is a string with the name to check.
	 * @r A boolean with true if it exists and false if not.
	 */
	public extern classExists(string ClassName);

	/**
	 * Gets the class definition for the provided
	 * class name. The return object is an instance of
	 * rclass, see it's definition for details.
	 * @p ClassName is a string with the class to get
	 * the definition of.
	 * @r An instance of rclass with the class definition.
	 */
	public extern getClassDef(string ClassName);

	/**
	 * Instantiates a new object of the class type provided.
	 * @p ClassName is a string with the class name to instantiate.
	 * @r A new instance of that class.
	 */
	public extern instantiate(string ClassName);

	/**
	 * Invokes a function with the provided object, method
	 * name, and optional arguments.
	 * @p Object is the object to invoke.
	 * @p MethodName is a name of the method to invoke.
	 * @p ... is an optional list of arguments to use as
	 * arguments when invoking the method.
	 * @r The value of the result of the invoked function.
	 */
	public extern invoke(object Object, string MethodName, ...);
}

/**
 * The rclass object is an object that is returned
 * when calling reflect.getClassDef(). It provides a way
 * to inspect the class definition.
 */
extern class rclass : com.aussom.stdlib.AClass {
	/**
	 * Gets the class name.
	 * @r A string with the class name.
	 */
	public extern getName();

	/**
	 * Gets the flag with if the class is static or not.
	 * @r A boolean with true if it's static and false if not.
	 */
	public extern isStatic();

	/**
	 * Gets the flag with the class is extern or not.
	 * @r A boolean with true if it's extern and false if not.
	 */
	public extern isExtern();

	/**
	 * Gets the external class name.
	 * @r A string with the external class name.
	 */
	public extern getExternClassName();

	/**
	 * Gets a map of members of the class with name and type.
	 * @r A map with name -> type of each member.
	 */
	public extern getMembers();

	/**
	 * Gets a map of methods with the class with method name
	 * method details. This function returns a whole structure
	 * with details about the method and it's arguments.
	 * @r A map of maps with the method and all it's details.
	 */
	public extern getMethods();
}
